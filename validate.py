from lxml import isoschematron
from lxml import etree
import requests
import os
import sys



class URIResolver(etree.Resolver):
    def resolve(self, url, pubid, context):
        try:
            open = requests.get(url)
            return self.resolve_string(open.text, context)
        except RuntimeError as RE:
            print("Recursed")

schematron_uri_parser = etree.XMLParser()
schematron_uri_parser.resolvers.add(URIResolver())


with open('schema/GEMINI_2.3_Schematron_Schema-v1.0.sch') as f:
    sct_doc = etree.parse(f,schematron_uri_parser)


##print(etree.tostring(sct_doc, pretty_print=True))
schematron = isoschematron.Schematron(sct_doc, store_report = True)


with open('schema/1044-ds.xml') as f:
    iso_gmd = etree.parse(f,schematron_uri_parser)


validation = schematron.validate(iso_gmd)

report = schematron.validation_report

print("is valid: " + str(validation))
print(type(report))
print(report)

